export declare class Greeter {
  greet(to: string): void;
}
export declare const Aaa: () => Promise<any>;
export declare const hello: (GRAPHQL_ENDPOINT: string) => void;
export declare const random: (GRAPHQL_ENDPOINT: string) => void;
export declare const dice: (GRAPHQL_ENDPOINT: string) => void;
