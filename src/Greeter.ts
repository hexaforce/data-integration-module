import { GET } from './Rest'

export class Greeter {
  greet(to: string): void {
    console.log(`Hello ${to}`)
  }
}

export const Aaa = async (): Promise<any> => {
  const response = await GET('/get');
  if (!response.ok) {
    return await response.text()
  }
  const result = await response.json();
  return result
}

export const hello = (GRAPHQL_ENDPOINT: string) => {
  fetch(`${GRAPHQL_ENDPOINT}/hello`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({ query: "{ hello }" }),
  })
    .then(r => r.json())
    .then(data => console.log("data returned:", data))
}

export const random = (GRAPHQL_ENDPOINT: string) => {
  fetch(`${GRAPHQL_ENDPOINT}/random`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({ query: "{ quoteOfTheDay, random }" }),
  })
    .then(r => r.json())
    .then(data => console.log("data returned:", data))
}

export const dice = (GRAPHQL_ENDPOINT: string) => {
  var dice = 3
  var sides = 6
  var query = `query RollDice($dice: Int!, $sides: Int) {
    rollDice(numDice: $dice, numSides: $sides)
  }`

  fetch(`${GRAPHQL_ENDPOINT}/dice`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({
      query,
      variables: { dice, sides },
    }),
  })
    .then(r => r.json())
    .then(data => console.log("data returned:", data))
}