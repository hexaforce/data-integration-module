export declare const VITE_API_ENDPOINT = "https://httpbin.org";
export declare const VITE_API_RETRY_COUNT = 3;
export declare const VITE_API_RETRY_DELAY = 1500;
export declare const GET: (path: string) => Promise<Response>;
export declare const POST: (path: string, body: any) => Promise<Response>;
export declare const PUT: (path: string, body: any) => Promise<Response>;
export declare const DELETE: (path: string) => Promise<Response>;
